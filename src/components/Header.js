import React from 'react'

import ReactTyped from 'react-typed';



class Header extends React.Component {
    render(){
        return(
            <>
                <div className="text-center">

                <div className="d-inline">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
width="96" height="96"
viewBox="0 0 172 172"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" ><path d="M0,172v-172h172v172z" fill="none"></path><g><path d="M146.91667,21.5h-121.83333c-2.15,0 -3.58333,1.43333 -3.58333,3.58333v125.41667h129v-125.41667c0,-2.15 -1.43333,-3.58333 -3.58333,-3.58333z" fill="#cfd8dc"></path><path d="M28.66667,46.58333h114.66667v96.75h-114.66667z" fill="#000000"></path><path d="M48.375,28.66667c-2.96853,0 -5.375,2.40647 -5.375,5.375c0,2.96853 2.40647,5.375 5.375,5.375c2.96853,0 5.375,-2.40647 5.375,-5.375c0,-2.96853 -2.40647,-5.375 -5.375,-5.375zM34.04167,28.66667c-2.96853,0 -5.375,2.40647 -5.375,5.375c0,2.96853 2.40647,5.375 5.375,5.375c2.96853,0 5.375,-2.40647 5.375,-5.375c0,-2.96853 -2.40647,-5.375 -5.375,-5.375z" fill="#90a4ae"></path><g fill="#e67e22"><path d="M66.29167,94.95833l-12.54167,-7.16667v-8.95833l21.5,12.18333v7.525l-21.5,12.54167v-8.95833zM82.41667,103.91667h35.83333v7.16667h-35.83333z"></path></g></g></g></svg>
                <h1 className="text-light title mt-3 d-inline align-middle" style={{ fontSize: '3.2rem' }} >Console snippets</h1>
                </div>
                </div>

                <h5 className="font-weight-light mt-3">

                <ReactTyped
                loop
                typeSpeed={100}
                backSpeed={40}
                strings={[" share the code", " share a command", " share it with Console-snippets."]}
                smartBackspace
                shuffle={false}
                backDelay={1}
                fadeOut={false}
                fadeOutDelay={100}
                loopCount={1}
                showCursor={false}
            />
            </h5>
          </>
        )
    }
}

export default Header
