import React from 'react'

import Header from './Header'

import html2canvas from "html2canvas";

import MacTheme from "../console-themes/MacTheme";
import WindowsClassicTheme from "../console-themes/WindowsClassicTheme";
import PowershellTheme from "../console-themes/PowershellTheme";
import UbuntuTheme from "../console-themes/UbuntuTheme";

class indexLayout extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = { 
            type: 'mac',
            mode: 'light',
            bg_color: "bg-success",
            disable_mode: false
        };
        this.terminal_theme = this.terminal_theme.bind(this);
        this.terminal_mode = this.terminal_mode.bind(this);
        this.bg_color = this.bg_color.bind(this);

    }
    
    takeScreenShot(e) {
      let image = document.getElementById('mockup');
          html2canvas(image, {
            scale:2.5
          }).then(canvas => {
                image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
                var link = document.createElement('a');
                link.download = "Console-snippet.png";
                link.href = image;
                link.click();
          });

      }

    ShowScreenshot(e) {

        window.scroll(0, 0);


    let image = document.getElementById('mockup');
        html2canvas(image, {
            scale:2.5
        }).then(canvas => {
            var imageblock = document.getElementById('image-block');
            if (imageblock.childNodes[0]){
                imageblock.removeChild(imageblock.childNodes[0]);
            }
            
            imageblock.appendChild(canvas);

        });

    }

    //Terminal style
    terminal_theme(e) {
        let value = e.target.value;
        if(e !== undefined){
            this.setState({ type: value });
        }
        (value === 'ubuntu') ? this.setState({ disable_mode: true }) : this.setState({ disable_mode: false });
    }
    terminal_mode(e) {
        let value = e.target.value;
        if(e !== undefined){
            this.setState({ mode: value });
        }
    }
    bg_color(e){
        if(e !== undefined){
            this.setState({ bg_color: e.target.name });
        }
    }

      render(){
        const renderConsole = () => {
            switch (this.state.type) {
                case 'mac':
                    return <MacTheme mode={this.state.mode} />
                case 'window_classic':
                    return <WindowsClassicTheme mode={this.state.mode} />
                case 'powershell':
                    return <PowershellTheme mode={this.state.mode} />
                case 'ubuntu':
                    return <UbuntuTheme mode={this.state.mode} />
                default:
                    return <MacTheme mode={this.state.mode} />
            }
        }
        return(
            
            <div>
                <div className="container mb-2 py-5">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <Header/>
                        </div>
                    </div>
                    <div className="container pt-5">
                        <div className="row">

                            <div className="responsiveCard card mb-4 d-md-none">
                                <div className="btn-group d-md-none d-flex" role="group">
                                    <button id="btnGroupExport" type="button" className="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Export
                                    </button>
                                    <div className="dropdown-menu mr-auto" aria-labelledby="btnGroupExport">
                                        <button className="dropdown-item" onClick={this.takeScreenShot}>Download</button>
                                        <button className="dropdown-item" onClick={this.ShowScreenshot} type="button" data-toggle="modal" data-target="#exampleModalCenter" >Open</button>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-2 d-none d-md-inline-block">
                                <div className="card">
                                    <div className="card-body">
                                        Share
                                        <div className="d-flex">
                                            <a class="btn-floating btn btn-tw" type="button" role="button" title="Share on facebook" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fconsole-snippets.com" target="_blank" rel="noopener">
                                                <img src="https://img.icons8.com/color/48/000000/facebook.png" alt="facebook-icon"/>
                                            </a>    

                                            <a class="resp-sharing-button__link mt-2" href="https://twitter.com/intent/tweet/?text=share%20commands%20with%20Console-snippets.&url=https%3A%2F%2Fconsole-snippets.com" target="_blank" rel="noopener" aria-label="Share on Twitter">
                                            <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                                <img src="https://img.icons8.com/fluent/48/000000/twitter.png" alt="twitter-icon"/>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                            <div className="col-md-8 text-center">
                                <div className="card rounded">
                                    <div id="console" className="rounded">
                                        <div id="mockup" className={"container p-5 rounded terminal " + this.state.bg_color}>
                                            <div className="card border-0 bg-white text-left border-none p-0 m-0 rounded" style={{maxHeight: '65rem'}}>
                                                {renderConsole()}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-2 my-4 mt-md-0">
                                <div className="responsiveCard card mb-3">
                                    <div className="btn-group d-none d-md-flex" role="group">
                                        <button id="btnGroupExport" type="button" className="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Export
                                        </button>
                                        <div className="dropdown-menu mr-auto" aria-labelledby="btnGroupExport">
                                            <button className="dropdown-item" onClick={this.takeScreenShot}>Download</button>
                                            <button className="dropdown-item" onClick={this.ShowScreenshot} type="button" data-toggle="modal" data-target="#exampleModalCenter" >Open</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card responsiveCard">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">
                                            <label for="customRange3">Theme</label>
                                            <select onChange={ this.terminal_theme} className="custom-select text-white">
                                                <option value="mac">Mac</option>
                                                <option value="window_classic">Windows classic</option>
                                                <option value="powershell">Windows Powershell</option>
                                                <option value="ubuntu">Ubuntu</option>
                                            </select>
                                        </li>
                                        <li className="list-group-item">
                                            <label for="customRange3">Console style</label>
                                            <select onChange={ this.terminal_mode} disabled={this.state.disable_mode} className="custom-select text-white">
                                                <option value="light">Light</option>
                                                <option value="dark">Dark</option>
                                            </select>
                                        </li>
                                        <li className="list-group-item">
                                            <label for="customRange3">Background color</label>
                                            <div className="d-flex">
                                                <button name="bg-primary" className=" bg-primary mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                                <button name="bg-danger" className="bg-danger mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                                <button name="bg-success" className="bg-success mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                                <button name="bg-info" className="bg-info mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                                <button name="bg-warning" className="bg-warning mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                                <button name="bg-secondary" className="bg-secondary mx-1 p-3 rounded-pill border-0" onClick= { this.bg_color }></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            <div class="modal fade" id="exampleModalCenter"   tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center" id="image-block">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onClick={this.takeScreenShot}>Download</button>
                    </div>
                    </div>
                </div>
            </div>


            <footer className="bg-dark text-primary pt-2">
                <div class="container">
                    <div class="row">
                    <div class="col text-center">
                        Console snippets © 2021
                    </div>
                        <div class="col text-center">
                            <a href="https://www.linkedin.com/in/camilo-php/" target="_blank"> 
                                <img src="https://img.icons8.com/fluency/48/000000/linkedin.png" height="35px" class="mx-2"/>
                            </a>
                            <a href="https://twitter.com/prog_camilo" target="_blank" > 
                                <img src="https://img.icons8.com/color/48/000000/twitter-circled--v1.png" height="35px" class="mx-2"/>
                            </a>
                        </div>
                    </div>
                </div>
            </footer>
            </div>
        )
    }
}
export default indexLayout
