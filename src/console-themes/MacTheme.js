import React from "react";


class MacTheme extends React.Component {
  constructor(props) {
    super(props);
  }
 
  render() {
    let mode = this.props.mode;

    if (mode == 'light'){
        return (
            <>
            <div className="background p-0 m-0 pl-2">
                <div className="row pt-3 mb-1">
                    <div className="col-4 d-flex flex-row">
                        <span className="close-icon-mac mx-1"></span>
                        <div className="max-icon-mac mx-1"></div>
                        <div className="min-icon-mac mx-1"></div>
                    </div>
                    <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-md-4 mb-1 text-center">
                        Root---.bash ---80x25
                    </div>
                    <div className="col-4">
                    </div>
                </div>
    
            </div>
            <div className="card-body">
                <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
                    Root-MacBook-Pro: ~ Root $ <span>Write HERE !</span>
                </div>
            </div>
            </>
        );
    }else{
        return (
            <>
            <div className="bg-black p-0 m-0 pl-2">
                <div className="row pt-3 mb-1">
                    <div className="col-4 d-flex flex-row">
                        <span className="close-icon-mac mx-1"></span>
                        <div className="max-icon-mac mx-1"></div>
                        <div className="min-icon-mac mx-1"></div>
                    </div>
                    <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-md-4 mb-1 text-center">
                        Root---.bash ---80x25
                    </div>
                    <div className="col-4">
                    </div>
                </div>
    
            </div>
            <div className="card-body bg-black">
                <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
                    Root-MacBook-Pro: ~ Root $ <span>Write HERE !</span>
                </div>
            </div>
            </>
        );

    }
  }
}

export default MacTheme
