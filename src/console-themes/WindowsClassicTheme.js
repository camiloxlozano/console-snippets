import React from "react";


class WindowsClassicTheme extends React.Component {
 
  render() {

    let mode = this.props.mode;

    if (mode == 'dark'){
        return (
        <>
        <div className="bg-black-1 p-0 m-0 pl-2">
            <div className="row pt-3 mb-1">
                <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6 mb-1">
                    Windows classic
                </div>

     
                <div className="col-6">
                    <div className="d-flex float-right mr-3">
                        <i class="fas fa-window-minimize mx-2"></i>
                        <i class="far fa-square mx-2"></i>
                        <i class="fas fa-times mx-2"></i>
                    </div>
                </div>
            </div>
        </div>
        <div className="card-body text-green bg-black">
            <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
              <p className="p0 m-0"> C:\users\programmer> <span>Write HERE !</span> </p>
            </div>
        </div>
        </>
        );
    } else {
        return (
            <>
            <div className="bg-white p-0 m-0 pl-2">
                <div className="row pt-3 mb-1">
                    <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6 mb-1">
                        Windows classic
                    </div>
    
                    <div className="col-6">
                        <div className="d-flex float-right mr-3">
                            <i class="fas fa-window-minimize mx-2"></i>
                            <i class="far fa-square mx-2"></i>
                            <i class="fas fa-times mx-2"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body text-light bg-black border-white">
                <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
                  <p className="p0 m-0"> C:\users\programmer: <span>Write HERE !</span> </p>
                </div>
            </div>
            </>
            );
    }
  }
}

export default WindowsClassicTheme