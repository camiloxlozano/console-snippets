import React from "react";


class UbuntuTheme extends React.Component {
 
  render() {

    return (
        <>
        <div className="container-fluid bg-gray text-light p-0 m-0 pl-2">
            <div className="row pt-3 mb-1 text-center">
              <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6 col-md-4 mb-1 d-md-none d-inline-block">
                  Ubuntu
              </div>

              <div className="col-4 d-none d-md-inline-block">

              </div>

                <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6  col-md-4 mb-1 d-none d-md-inline-block">
                    Ubuntu
                </div>

                <div className="col-6 col-md-4 text-left">
                  <div className="d-flex float-right mr-3 pb-2">
                        <i class="fas fa-window-minimize mx-3 mt-1"></i>
                        <i class="far fa-square mx-3 mt-2"></i>
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 172 172" ><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e67e22"><path d="M86,17.2c-37.9948,0 -68.8,30.8052 -68.8,68.8c0,37.9948 30.8052,68.8 68.8,68.8c37.9948,0 68.8,-30.8052 68.8,-68.8c0,-37.9948 -30.8052,-68.8 -68.8,-68.8zM112.9868,104.87987c2.24173,2.24173 2.24173,5.8652 0,8.10693c-1.118,1.118 -2.58573,1.67987 -4.05347,1.67987c-1.46773,0 -2.93547,-0.56187 -4.05347,-1.67987l-18.87987,-18.87987l-18.87987,18.87987c-1.118,1.118 -2.58573,1.67987 -4.05347,1.67987c-1.46773,0 -2.93547,-0.56187 -4.05347,-1.67987c-2.24173,-2.24173 -2.24173,-5.8652 0,-8.10693l18.87987,-18.87987l-18.87987,-18.87987c-2.24173,-2.24173 -2.24173,-5.8652 0,-8.10693c2.24173,-2.24173 5.8652,-2.24173 8.10693,0l18.87987,18.87987l18.87987,-18.87987c2.24173,-2.24173 5.8652,-2.24173 8.10693,0c2.24173,2.24173 2.24173,5.8652 0,8.10693l-18.87987,18.87987z"></path></g></g></svg>
                    </div>
                </div>
            </div>
        </div>
        <div className="card-body text-white bg-purple">
            <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
              <p className="p0 m-0"> C:\users\programmer> <span>Write HERE !</span> </p>
            </div>
        </div>
        </>
    );
  }
}

export default UbuntuTheme