import React from "react";


class PowershellTheme extends React.Component {
 
  render() {
    let mode = this.props.mode;

    if (mode == 'light'){
        return (
            <>
            <div className="bg-blue text-white p-0 m-0 pl-2">
                <div className="row pt-3 mb-1">
                    <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6 mb-1">
                        PowerShell
                    </div>

                    <div className="col-6">
                        <div className="d-flex float-right mr-3">
                            <i class="fas fa-window-minimize mx-2"></i>
                            <i class="far fa-square mx-2"></i>
                            <i class="fas fa-times mx-2"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body text-white bg-blue-dark border-blue">
                <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
                  <p className="p0 m-0">PS C:\users\programmer {'>'} <span>Write HERE !</span> </p>
                </div>
            </div>
            </>
        );
    }else{
        return (
            <>
            <div className="bg-black text-white p-0 m-0 pl-2">
                <div className="row pt-3 mb-1">
                    <div contentEditable="true" autoCorrect="off" autoComplete="off" className="col-6 mb-1">
                        PowerShell
                    </div>
    
                    <div className="col-6">
                        <div className="d-flex float-right mr-3">
                            <i class="fas fa-window-minimize mx-2"></i>
                            <i class="far fa-square mx-2"></i>
                            <i class="fas fa-times mx-2"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body text-white bg-blue-dark border-black">
                <div className="py-2" contentEditable="true" autoCorrect="off" autoComplete="off">
                  <p className="p0 m-0">PS C:\users\programmer> <span>Write HERE !</span> </p>
                </div>
            </div>
            </>
        );
    }

  }
}

export default PowershellTheme