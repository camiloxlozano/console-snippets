import './App.css';
import IndexLayout from './components/IndexLayout'
import ReactGA from 'react-ga'

function initializeAnalitics(){
  ReactGA.initialize("G-Z7D43YH89M")
  ReactGA.pageview(window.location.pathname + window.location.search);
}

function App() {
  initializeAnalitics()
  return (
    <div className="App">
      <IndexLayout />
    </div>
  );
}

export default App;
